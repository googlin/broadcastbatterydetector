package com.stanlytango.android.broadcastapp2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BatteryFragment extends Fragment {

    private ProgressBar bar = null;
    private ImageView status = null;
    private TextView level = null;

    private BroadcastReceiver onBattery = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int pct =
                    100 * intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 1)
                            / intent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);
            bar.setProgress(pct);
            level.setText(""+ pct + " %");

            switch (intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)) {
                case BatteryManager.BATTERY_STATUS_CHARGING:
                    status.setImageResource(R.drawable.charging_50_black_24dp);
                    break;
                case BatteryManager.BATTERY_STATUS_FULL:
                    int plugged =
                            intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
                    if (plugged == BatteryManager.BATTERY_PLUGGED_AC
                            || plugged == BatteryManager.BATTERY_PLUGGED_USB) {
                        status.setImageResource(R.drawable.charging_full_black_24dp);
                    } else {
                        status.setImageResource(R.drawable.full_black_24dp);
                    }
                    break;
                default:
                    status.setImageResource(R.drawable.unplugged_black_24dp);
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.batt, container, false);
        bar = view.findViewById(R.id.bar);
        status = view.findViewById(R.id.status);
        level = view.findViewById(R.id.level);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        getActivity().registerReceiver(onBattery, intentFilter);
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(onBattery);
        super.onStop();
    }
}
